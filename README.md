![Python Version](https://img.shields.io/badge/python-3.7%20%7C%203.8-blue) [![GitLab license](https://img.shields.io/github/license/Naereen/StrapDown.js.svg)](https://gitlab.inria.fr/dh-projects/kami/kami-cli/-/blob/master/LICENSE) 

# KaMi CLI

<!--![KaMI lib logo](./docs/static/kramin_carmin_lib.png)-->

<img src="./docs/static/kramin_carmin_cli.png" alt="KaMI lib logo" height="100" width ="100"/>
Python CLI based on [Kami-lib](https://gitlab.inria.fr/dh-projects/kami/kami-lib) to evaluate HTR/OCR models "on-the-fly" or with batch processes and export results in CSV.


### Dependencies 

Kami requires : 

* Python (<=3.8)
* kamilib (==0.1.1)

In progress...
