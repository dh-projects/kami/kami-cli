#!/bin/bash

# Author : Lucas Terriel <lucas.terriel@inria.fr>
# Date : 27/08/2021
#
# Tests script including the main commands of Kami cli
# Always run this tests locally not in VM (prompt actions !)

# Features :
# 1) Add a switch case to access to specific test
# 2) Add an option to run all tests in switch case


stop_and_start () {
  read -p "Continue? (Y/N): " confirm && [[ $confirm == [yY] || $confirm == [yY][eE][sS] ]] || exit 1
}

chmod +x kamicli.py
echo "** START TEST MODE **"

echo "-> TEST 1 ) Simple : text / image / model and no transforms options"

./kamicli.py datatest/text_jv/GT_1.txt -i datatest/images_jv/Voyage_au_centre_de_la_[...]Verne_Jules_btv1b8600259v_16.jpeg -m datatest/on_hold/KB-app_model_JulesVerne1_best.mlmodel

stop_and_start

echo "-> TEST 2 ) Simple : text / image / model with transforms options"

./kamicli.py datatest/text_jv/GT_1.txt -i datatest/images_jv/Voyage_au_centre_de_la_[...]Verne_Jules_btv1b8600259v_16.jpeg -m datatest/on_hold/KB-app_model_JulesVerne1_best.mlmodel -t XP

stop_and_start

echo "-> TEST 3 ) Simple : xml / image / model and no transforms options"

./kamicli.py datatest/medium_xml_page/FRAN_0150_0002_L-medium_1.xml -i datatest/medium_images/FRAN_0150_0002_L-medium_1.jpg -m datatest/models/model_tapuscrit_n2.mlmodel

stop_and_start

echo "-> TEST 4 ) Simple : xml / image / model with transforms options"

./kamicli.py datatest/medium_xml_page/FRAN_0150_0002_L-medium_1.xml -i datatest/medium_images/FRAN_0150_0002_L-medium_1.jpg -m datatest/models/model_tapuscrit_n2.mlmodel -t XPD

stop_and_start

echo "-> TEST 5 ) batch : text / image / model and no transforms options"

./kamicli.py datatest/text_jv -i datatest/images_jv -m datatest/on_hold/KB-app_model_JulesVerne1_best.mlmodel

stop_and_start

echo "-> TEST 6 ) batch : text / image / model with transforms options"

./kamicli.py datatest/text_jv -i datatest/images_jv -m datatest/on_hold/KB-app_model_JulesVerne1_best.mlmodel -t XPD

stop_and_start

echo "-> TEST 7 ) batch : xml / image / model and no transforms options"

./kamicli.py datatest/medium_xml_page -i datatest/medium_images -m datatest/models/model_tapuscrit_n2.mlmodel

stop_and_start

echo "-> TEST 8 ) batch : xml / image / model with transforms options"

./kamicli.py datatest/medium_xml_page -i datatest/medium_images -m datatest/models/model_tapuscrit_n2.mlmodel -t XPD

stop_and_start

echo "-> TEST 9 ) batch : text / image / model with transforms options + export CSV"

./kamicli.py datatest/text_jv -i datatest/images_jv -m datatest/on_hold/KB-app_model_JulesVerne1_best.mlmodel -t XPD -c

stop_and_start

echo "-> TEST 10 ) batch : xml / image / model with transforms options + export CSV"

./kamicli.py datatest/medium_xml_page -i datatest/medium_images -m datatest/models/model_tapuscrit_n2.mlmodel -t XPD -c

stop_and_start

echo "** END TEST MODE **"