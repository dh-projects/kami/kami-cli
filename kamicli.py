#!/usr/bin/env python
#-*- coding: utf-8 -*-

import csv
from datetime import datetime
from os import listdir
from os.path import isdir, isfile, basename
import re
import sys

import click
from kami.Kami import Kami
from kami.kamutils._utils import _report_log
import pandas as pd
import pyfiglet
from tabulate import tabulate
from tqdm import tqdm


HEADERS_METRICS = ["default",
                   "all_transforms",
                   "non_digits",
                   "uppercase",
                   "lowercase",
                   "remove_punctuation",
                   "remove_diacritics"]

HEADERS_METADATAS = ["Length_reference",
                     "Length_prediction",
                     "Length_reference_transformed",
                     "Length_prediction_transformed"
                     "Total_char_removed_from_reference",
                     "Total_char_removed_from_prediction",
                     "Total_diacritics_removed_from_reference",
                     "Total_diacritics_removed_from_prediction",
                     "Total_char_pass_in_uppercase_in_reference",
                     "Total_char_pass_in_uppercase_in_prediction",
                     "Total_char_pass_in_lowercase_in_reference",
                     "Total_char_pass_in_lowercase_in_prediction"]


CODES_TRANSFORMS = {
                "D": "Remove digits",
                "U": "Uppercase",
                "L": "Lowercase",
                "P": "Remove punctuation",
                "X": "Remove diacritics"
            }



# I/O utils
def _list_files(path_dir : str) -> list:
    return listdir(path_dir)

#TODO(): Refactor _export_csv() for no batch
def _export_csv(name_csv: str, pairs_meta_metrics: list) -> None:
    """Export batch results
    :param name_csv:
    :param pairs_meta_metrics:
    :return:
    """
    # create a blank csv
    csv_file = open(name_csv, mode='w')
    csv_file.close()
    for tuple in pairs_meta_metrics:
        meta = tuple[0]
        df = tuple[1]
        # a - Add metadata to new csv
        for key, value in meta.items():
            with open(name_csv, mode='a+') as csv_file:
                writer = csv.writer(csv_file, delimiter=',')
                writer.writerow([key, value])

        # b- Add HTR/OCR scores
        df.to_csv(name_csv, mode='a', header=True)

        # c- create a blank row in csv for new part
        csv_file = open(name_csv, mode='a+')
        writer = csv.writer(csv_file, delimiter=',')
        writer.writerow([])
        csv_file.close()

# Data structures manipulation utils
def _get_list_tuple(list_1: list, list_2: list) -> list:  # TODO refactor
    """returns sort of list containing tuples made up of elements from two or three lists
    """
    list_tuple = [(list_1[index],
                       list_2[index]) for index in range(min(len(list_1),
                                                             len(list_2)))]

    return list_tuple





@click.argument('ground_truth')
@click.option('--prediction', '-p', default='', type=str, help='Pass a string as prediction, prediction file (XML or text) or absolute path to dir contains prediction files.')
@click.option('--image', '-i', default='', type=str, help='Path to image file or a directory containing images (in the same order as GROUND TRUTH dir).')
@click.option('--model', '-m', default='', type=str, help='Path to HTR/OCR Kraken model (.mlmodel extension).')
@click.option('--text_processing', '-t', default='', type=str, help='Options for text processing and modulate results.')
@click.option('--verbosity', '-v', default=False, type=bool, help='')
@click.option('--export_csv', '-c', is_flag=True, help='')
@click.command()
def cli(ground_truth, prediction, image, model, text_processing, verbosity, export_csv):
    """A mini CLI based on Kamilib

    GROUND-TRUTH is a string, path to a file (XML/text), or directory containing files (in the same order as images) as ground truth
    """
    NOW = datetime.now()
    DATETIME = NOW.strftime("%d_%m_%Y_%H:%M:%S")
    # ~ Introduction to CLI
    LOGO = pyfiglet.figlet_format('KAMI CLI')
    print(LOGO)
    print(f"[{DATETIME}] CLI based on Kamilib to evaluate HTR/OCR Models - © 2021\n")
    # ~ Display options choose by user
    print("- Global configuration : \n")
    if text_processing:
        choices_preprocessing = " | ".join([letter for letter in text_processing])
        table_transforms = choices_preprocessing.maketrans(CODES_TRANSFORMS)
        _report_log(f"* Text processing option(s) : {choices_preprocessing.translate(table_transforms)}", type_log='W')
    else:
        _report_log(f"* Text processing option(s) : {text_processing}", type_log='I')
    if export_csv:
        _report_log(f"* Export csv option : {export_csv}", type_log='W')
    else:
        _report_log(f"* Export csv option : {export_csv}", type_log='I')
    if verbosity:
        _report_log(f"* Verbosity option : {verbosity}\n", type_log='W')
    else:
        _report_log(f"* Verbosity option : {verbosity}\n", type_log='I')


    # ~ Process and orient input
    if isfile(ground_truth) and image and model:
        # User want to evaluate single file (XML/TEXT) with image and model (with transforms or not)
        kevaluator = Kami(ground_truth,
                            model=model,
                            image=image,
                            apply_transforms=text_processing,
                            verbosity=verbosity,
                            truncate=True,
                            percent=True,
                            round_digits='0.01')

        k_board = kevaluator.scores.board

        dict_metadatas = {}

        if text_processing:
            dict_metrics = {key: k_board[key] for key in HEADERS_METRICS if key in k_board.keys()}
            dict_metadatas = {key: value for key, value in k_board.items() if key in HEADERS_METADATAS}
            df_metrics = pd.DataFrame.from_dict(dict_metrics)
        else:
            df_metrics = pd.DataFrame.from_dict(k_board, orient='index', columns=['HTR/OCR Scores'])

        if click.confirm('Do you show reference and prediction ?'):
            print(f"""
                        Reference
                        =========

                        {kevaluator.reference}

                        Prediction
                        ==========

                        {kevaluator.prediction}""")
        if text_processing:
            if click.confirm('Do you show reference and prediction with transforms ?'):
                print(f"""
                        Reference (with transforms)
                        ===========================

                        {kevaluator.reference_preprocess}

                        Prediction (with transforms)
                        ============================

                        {kevaluator.prediction_preprocess}""")

        _report_log(f"************* EVALUATION HTR/OCR REPORT FROM KAMI - {DATETIME} *************", type_log='S')

        # a - Display metadatas
        for key, value in dict_metadatas.items():
            print(f"{key} : {value}")

        # b- Display metrics table
        print(tabulate(df_metrics, headers='keys', tablefmt='psql'))

    elif isdir(ground_truth):
        # User want to evaluate batch files (XML/TEXT) with images and model (with transforms or not)
        if isdir(image):
            if not ground_truth.endswith('/'):
                ground_truth = ground_truth + '/'
            if not image.endswith('/'):
                image = image + '/'
            if model:
                root_gt = ground_truth
                root_image = image
                results = []
                combine = _get_list_tuple(sorted(_list_files(ground_truth)), sorted(_list_files(image)))
                if verbosity:
                    for tuple in combine:
                        gt_file = root_gt + tuple[0]
                        image_file = root_image + tuple[1]
                        kevaluator = Kami(gt_file,
                              model=model,
                              image=image_file,
                              apply_transforms=text_processing,
                              verbosity=verbosity,
                              truncate=True,
                              percent=True,
                              round_digits='0.01')
                        k_board = kevaluator.scores.board
                        results.append((tuple[0], tuple[1], k_board))
                else:
                    for tuple in tqdm(combine):
                        gt_file = root_gt + tuple[0]
                        image_file = root_image + tuple[1]
                        kevaluator = Kami(gt_file,
                              model=model,
                              image=image_file,
                              apply_transforms=text_processing,
                              verbosity=verbosity,
                              truncate=True,
                              percent=True,
                              round_digits='0.01')
                        k_board = kevaluator.scores.board
                        results.append((tuple[0], tuple[1], k_board))

                # Create uniques data structures to output processes
                meta_dicts = []
                metrics_dicts = []
                metrics_dataframes = []

                for tuple in results:
                    dict_metadatas = {}
                    dict_metadatas['GROUND_TRUTH'] = tuple[0]
                    dict_metadatas['IMAGE'] = tuple[1]
                    if text_processing:
                        for key, value in tuple[2].items():
                            if key in HEADERS_METADATAS:
                                dict_metadatas[key] = value

                    #dict_metadatas = {key: tuple[2][key] for key in HEADERS_METADATAS if key in tuple[2].keys()}
                    meta_dicts.append(dict_metadatas)
                    dict_metrics = {key: tuple[2][key] for key in HEADERS_METRICS if key in tuple[2].keys()}
                    # cause by user not enter any transformations
                    if len(dict_metrics) == 0:
                        dict_metrics = tuple[2]
                    metrics_dicts.append(dict_metrics)
                    # create metrics dataframe
                    try:
                        df_metrics = pd.DataFrame.from_dict(dict_metrics)
                        metrics_dataframes.append(df_metrics)
                    except:
                        df_metrics = pd.DataFrame.from_dict(dict_metrics, orient='index', columns=['HTR/OCR Scores'])
                        metrics_dataframes.append(df_metrics)

                # pairs of metadatas and metrics dataframes
                pairs_meta_metrics = _get_list_tuple(meta_dicts, metrics_dataframes)



                # ~ Output (1) : Display file metadatas and results in table format
                # Display global title
                print("\n")
                _report_log(f"************* EVALUATION HTR/OCR REPORT FROM KAMI - {DATETIME} / MODEL : {basename(model)} *************\n", type_log='S')

                for tuple in pairs_meta_metrics:
                    # a - Display metadatas
                    for key, value in tuple[0].items():
                        print(f"{key} : {value}")
                    # b- Display metrics table
                    print(tabulate(tuple[1], headers='keys', tablefmt='psql'))

                # ~ Output (2) : Export csv
                if export_csv:
                    invalid_csv = False
                    dir_csv = ''
                    # Enter the path where store directory and test if the dirpath exist
                    while invalid_csv is False:
                        dir_csv = click.prompt('Indicate directory path where you should store CSV or abort with crtl+c/crtl+z')
                        invalid_csv = isdir(dir_csv)
                        if invalid_csv is False:
                            print("Invalid directory. Please try again.")

                    # If exists write the csv, test and arrange path
                    if dir_csv.endswith('/'):
                        name_csv = dir_csv + f"evaluation_report_kami_{DATETIME}_{basename(model)}.csv"
                    else:
                        name_csv = dir_csv + '/' + f"evaluation_report_kami_{DATETIME}_{basename(model)}.csv"
                    try:
                        _export_csv(name_csv, pairs_meta_metrics)
                        _report_log(f"{basename(name_csv)} export in directory : {dir_csv}", type_log='S')
                    except:
                        _report_log(f"Export csv fail", type_log='E')

    else:
        if len(ground_truth) == 0:
            print("It seems your ground truth sentence is empty. Please retry.")
            sys.exit()
        if len(prediction) == 0:
            print("It seems your prediction sentence is empty. Please retry.")
            sys.exit()
        # User want to evaluate two arbitrary text files without model (with transforms or not)
        # User want to evaluate two arbitrary strings without model (with transforms or not)
        kevaluator = Kami([ground_truth, prediction],
                          apply_transforms=text_processing,
                          verbosity=verbosity,
                          truncate=True,
                          percent=True,
                          round_digits='0.01')
        
        k_board = kevaluator.scores.board

        dict_metrics = {key: k_board[key] for key in HEADERS_METRICS if key in k_board.keys()}
        dict_metadatas = {}

        if text_processing:
            dict_metadatas = {key:value for key, value in k_board.items() if key in HEADERS_METADATAS}
            df_metrics = pd.DataFrame.from_dict(dict_metrics)
        else:
            df_metrics = pd.DataFrame.from_dict(dict_metrics, orient='index', columns=['HTR/OCR Scores'])

        if click.confirm('Do you show reference and prediction ?'):
            print(f"""
                        Reference
                        =========
        
                        {kevaluator.reference}
        
                        Prediction
                        ==========
        
                        {kevaluator.prediction}""")
        if text_processing:
            if click.confirm('Do you show reference and prediction with transforms ?'):
                print(f"""
                        Reference (with transforms)
                        ===========================

                        {kevaluator.reference_preprocess}

                        Prediction (with transforms)
                        ============================

                        {kevaluator.prediction_preprocess}""")

        _report_log(f"************* EVALUATION HTR/OCR REPORT FROM KAMI - {DATETIME}", type_log='S')


        # a - Display metadatas
        for key, value in dict_metadatas.items():
            print(f"{key} : {value}")

        # b- Display metrics table
        print(tabulate(df_metrics, headers='keys', tablefmt='psql'))


if __name__ == '__main__':
    cli()
